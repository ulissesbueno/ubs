$(function(){
	
	var game = new ubs( $('#game') );

	game.displayLabelScore = $('#score')
	game.displayLabelChances = $('.icon.counter');
	game.displayLabelLevel = $('#level');

	game.onStart = function(){
		$('.footer .icon').removeClass('disabled')
		$('#game').removeClass('disabled');
		$('#StopLevel').hide( 200 )
		$('#GameOver').hide()
	}
	game.onEndChanges = function(){
		$('.footer .icon').addClass('disabled')
	}
	game.onEndLevel = function(){
		$('#game').addClass('disabled');
		$('#StopLevel').show( 200 )
	}
	game.onStatus = function( st ){
		$('#status .pices').html('');
		var ul = $('<ul>')
		for( var s in st['amounts'] ){
			var li = $('<li>')

			for( var p in game.pices ){
				if( game.pices[p].num == s ){
					pic = game.pices[p];
					break;
				}
			}

			var html = "";
				html += " <div class='pice "+pic.icon+" '><div class='icon'></div></div> <div class='amount'> "+ st['amounts'][s] +"</div> ";

			li.html( html )
			ul.append(li)
		}
		$('#status .pices').append(ul).append("<div style='clear:both'></div>")
	}
	game.onGameOver = function( st ){
		$('#game').addClass('disabled');
		var go = $('#GameOver');
		var pos = go.position()
		var top = go.top;
		go.css({top:0});
		go.show(300)
		go.animate({ 'top' : top }, 300)
	}

	game.start();
	//game.view( $('#view') );
	$('.footer .help-rr').click(function(){
		game.help('RR')
	})
	$('.footer .help-rl').click(function(){
		game.help('RL')
	})
	$('.footer .help-x').click(function(){
		game.help('X')
	})

	$('#controle .save').click(function(){
		game.save()
	})

	$('#controle .load').click(function(){
		game.load()
	})

	$('.next').click(function(){
		game.nextLevel()
	})

	$('.restart').click(function(){
		game.restart()
	})

	$(window).resize(function(){
		RePosition()
	}).resize()

	$(window).bind('keydown', function(event) {
	    if (event.ctrlKey || event.metaKey) {
	        switch (String.fromCharCode(event.which).toLowerCase()) {
	        case 's':
	            event.preventDefault();
	            game.save()
	            break;
	        case 'o':
	            event.preventDefault();
	            game.load()
	            break;
	        }

	        return false;
	    }
	});
})



function RePosition(){
	var m = $('#main')
	var pos = m.position();
	$('#main').css({ top : ($( window ).height() / 2) - ($('#main').outerHeight() / 2),
					 left : ($( window ).width() / 2) - ($('#main').outerWidth() / 2) })
}
