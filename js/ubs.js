var ubs = function( where ){

	/*Propriedades iniciais*/
	this.level = 1;
	this.max_level = 8;
	this.level_time_init = 60;
	this.matrix = [];

	/* Theme � a pasta do css para estilizar o jogo */
	this.pices = 	[	
						{ 'num':1, 'icon':'one' },
						{ 'num':2, 'icon':'two' },
						{ 'num':3, 'icon':'three' }
					];

	// Combinar 3 pe�as
	this.num_comb = 3;
	// Dimens�o inicial
	this.cols = 5;
	this.rows = 5;
	// Quantidade de ajudas
	this.help_num = 3;

	// Ponto do Jogo
	this.point = 100;

	// Displays
	this.displayLabelScore = null;
	this.displayLevel = null;	
	this.displayLabelChances = null;

	this.status = [];
	this.matrix_tmp = [];

	// Eventos
	this.onStart = null;
	this.onEndChanges = null;
	this.onEndLevel = null;
	this.onStatus = null;
	this.onGameOver = null;

	// Propriedades auxiliares
	this.comb = [];
	this.count = 0;	
	this.icon_size = { w : 48, h : 48, p : 10 }
	this.remove = [];
	this.score = 0;
	this.gaming = false;


	var me = this;

	// Come�ar
	this.start = function(){

		me.status['amounts'] = [];
		for( var i in me.pices ){
			me.status['amounts'][ me.pices[i].num ] = 0;
		}

		me.grid( me.cols, me.rows );
		me.draw( where );
		me.displayLabelChances.html( me.help_num )
		if( me.onStart ){
			me.onStart()
		}

		me.gaming = true;
		me.displayScore();
		me.displayLevel();
	}

	// Recome�ar
	this.restart = function(){
		me.matrix = '';
		me.matrix = [];

		me.level = 1;
		me.num_comb = 3;
		me.cols = 5;
		me.rows = 5;
		me.help_num = 3;
		me.gaming = false;
		me.score = 0;
		if(me.onStatus)me.onStatus( me.status )
		me.displayScore();

		me.start();
	}

	// Criar o grid
	this.grid = function( cols, rows ){

		var pices = me.shufle( cols, rows );

		for( var l = 0, p = 0; l < rows; l++  ){
			me.matrix[l] = [];
			for( var c = 0; c < cols; c++ ){
				me.status['amounts'][pices[p].num] ++;
				me.matrix[l][c] = { 'num': pices[p].num, 'icon' : pices[p].icon };
				p++
			}
		}

		if(me.onStatus)me.onStatus( me.status )

	}
	
	// Embaralhar
	this.shufle = function( cols, rows ){
		
		var total = cols * rows;
		var tp = me.pices.length;
		var t = 0;
		var deck = [];
		
		var max = 0;
		var min = 100;
		var max_i = 0;
		var min_i = 0;

		for( var i = 0; i < tp; i ++ ){
			var mx = Math.round( ( 40 * total ) / 100 );
			var r = Math.round((Math.random() * mx ));
			r = ( r < 3 ? r = 3: r = r );

			if( r > max ){
				max = r;	
				max_i = i
			} 
			if( r < min ){
				min = r;	
				min_i = i
			} 

			deck.push( r )

			t += r;			
		}

		if( t > total ){
			var dif = t - total;
			deck[max_i] -= dif;
		}
		if( t < total ){
			var dif = total - t;
			deck[min_i] += dif;
		}

		var new_deck = [];
		for( var i in deck ){
			for( var p = 0; p < deck[i]; p++  ){
				new_deck.push( me.pices[i] )
			}
		}

		new_deck = me.shuffle_ar( new_deck );
		//$('#view').html( mx+"<br>"+new_deck )
		return new_deck;
	}

	// Embaralhar array
	this.shuffle_ar = function(array) {
	  var currentIndex = array.length, temporaryValue, randomIndex;

	  // While there remain elements to shuffle...
	  while (0 !== currentIndex) {

	    // Pick a remaining element...
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;

	    // And swap it with the current element.
	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
	  }

	  return array;
	}


	// Ver matriz
	this.view = function( div ){
		div.html('')
		for( var l = 0; l < me.matrix.length ; l++  ){
			for( var c = 0; c < me.matrix[l].length; c++ ){
				div.append( me.matrix[l][c].num );
			}
			div.append( "<br>" );
		}
	}

	// Clicar numa pe�a
	this.shoot = function( x, y ){

		if( me.gaming ){						
			
			if( me.testShoot( x,y) ){
				me.setScore( me.count );

				for( var r in me.remove ){
					me.goOut(me.matrix[me.remove[r][1]][me.remove[r][0]])
				}
			
				me.justify();
				//me.view( $('#view') );	
				me.anime( where )
				me.remove = 0;
				me.remove = []

				// Acabou ?				
				me.isTheEnd()
				
				
			} else {
				//me.matrix = bkp;
				me.setOut();

			}
			
			me.count = 0;
			//me.view( $('#view') );
		}

		if(me.onStatus)me.onStatus( me.status )		
	}

	// Teste de clique
	this.testShoot = function( x, y ){
		
		me.count = 0;
		me.comb = '';
		me.comb = [];
		me.remove = '';
		me.remove = [];

		me.matrix_tmp = '';
		me.matrix_tmp = [];

		for( var l in me.matrix ){
			me.matrix_tmp[l] = [];
			for( var c in me.matrix[l] ){
				me.matrix_tmp[l][c] = me.matrix[l][c].num;
			}
		}
		
		var my_val = me.matrix_tmp[y][x];

		me.around( x,y, my_val);		
		while( me.comb.length ){
			me.around( Number(me.comb[0][0]), Number(me.comb[0][1]), my_val );
			me.comb.shift()			
		}

		return (me.count >= me.num_comb);
	}

	// Verifica em volta da pe�a 
	this.around = function( x, y, my_val ){
		
		//$('#status').append( 'click: '+x+' - '+y+"<br>" )
		if( my_val ){
			// up
			
			if( y > 0 ){
				me.chekcoord( x, y-1, my_val );
			}
			//down
			if( y < ( me.rows -1 ) ){
				me.chekcoord( x, y+1, my_val );
			}
			//right
			if( x < ( me.cols -1 ) ){
				me.chekcoord( x+1, y, my_val );				
			}
			//left
			if( x > 0  ){
				me.chekcoord( x-1, y, my_val );
			}
		}

	}

	// Checar a cordenada passada
	this.chekcoord = function( x, y, my_val ){
		//$('#status').append( x+' - '+y+" : "+ me.matrix[y][x].num +" = "+my_val+"<br>" )
		if( me.matrix_tmp[y][x] == my_val &&
			me.matrix_tmp[y][x] != 'X' ){
			
			me.matrix_tmp[y][x] = 'X';
			
			me.count ++;
			me.comb.push([x,y]);			
			me.remove.push([x,y]);
		} 
	}

	// Ajustar pe�as baixo e direita
	this.justify = function(){
		
		// DOWN
		var coluns = me.cols;
		var _col_ = 0;
		var ar_x = new Array( me.rows )
		ar_x.fill({'num':'X','icon':'','element':null})
		
		var num_col = [];

		for( var col = 0; col < coluns; col++ ){
			
			_col_ = '';
			_col_ = []

			for( var y in me.matrix ){
				if( me.matrix[y][col].element ) _col_.push( me.matrix[y][col] );
			}

			if( _col_.length ){				
				_col_ = ar_x.concat( _col_ ).slice( me.rows*-1 );
				for( var py in _col_ ){
					me.matrix[py][col] = {'num' : _col_[py].num, 'icon' : _col_[py].icon, 'element' : _col_[py].element};
				}
			} else {
				num_col.push(col);
			}

		}	

		// limpa colunas vazias
		if(num_col.length){

			var new_matrix = [];
			for( var l in me.matrix ){
				new_matrix[l] = [];
				for( var c in me.matrix[l] ){
					if( me.has( num_col,  c )  ){
						new_matrix[l].unshift({'num':'X','icon':'','element':null})
						continue;
					} else {	
						new_matrix[l][c] = me.matrix[l][c];
					}
				}
			}

			me.matrix = new_matrix;
		}
		
	}

	// 
	this.has = function( ar, v ){
		for( i in ar ){
			if( ar[i] == v ) return true;
		}
		return false;
	}

	// Desenhar pe�as do tabuleiro
	this.draw = function( where ){

		where.html('')

		for( var l = 0; l < me.matrix.length ; l++  ){

			for( var c = 0; c < me.matrix[l].length; c++ ){
				
				var div = $("<div>")
				if( c == 0)div.addClass('first')


				var ww = where.width() ;
				var ww_ = ww / me.cols;

				var hw = where.height() ;
				var hw_ = hw / me.rows;

				me.icon_size.w = ww_
				me.icon_size.h = hw_

				div.css({
					width: me.icon_size.w,
					height: me.icon_size.h
				})

				div.attr('px',c)
				div.attr('py',l)
				div.addClass('pice');
				div.addClass( me.matrix[l][c].icon );
				div.attr('data-coord',String(c)+String(l)) 
				div.html("<div class='icon'>&nbsp;</div>")

				var icon = div.find('.icon');
				/*
				var degrees = Math.random() * 360;
				icon.css({'transform': 'rotate('+ degrees +'deg)'} )
				*/

				var pos = me.position( c, l )
				div.css({
					'left' : pos.px ,
					'top' : 0 ,
					'opacity': 0
				})

				if( me.matrix[l][c].num == 'X' ){
					continue;
				}

				//div.html( '&nbsp;' );
				where.append( div );
				

				me.matrix[l][c].element = div;
			}
		}
		/*
		where.css({ width : (me.icon_size.w  +  me.icon_size.p) * me.cols,
					height: (me.icon_size.h  +  me.icon_size.p) * me.rows })
					*/
		where.append("<div style='clear:both'></div>")

		$('.pice').unbind('click').click(function(){
			me.shoot( Number($(this).attr('px')), Number($(this).attr('py')) );
		});

		me.anime( where );

	}

	// Fazer movimento das pe�as
	this.anime = function( where ){
		var speed = 200;
		
		for( var l = 0; l < me.matrix.length ; l++  ){
			for( var c = 0; c < me.matrix[l].length; c++ ){				

				if( me.matrix[l][c].num == 'X' ) continue;

				var element = me.matrix[l][c].element;

				element.attr('data-coord',String(c)+String(l)) ;
				element.attr('px',c)
				element.attr('py',l)

				if( element ){					
					var pos = me.position( c, l )
					element.animate( {
						'top' : pos.py,
						'left' : pos.px,
						'opacity' : 1
						
					},speed )	
				}
				speed += 2
			}
		}
	}

	// Jogar pe�as fora
	this.goOut = function( mtx ){

		var rand = Math.round( Math.random() * 500 ) + 100

		var pic = mtx.element;
		mtx.element = null;
		var pos = pic.position;
		pic.animate( {
			'top' : 0,
			'opacity' : 0
		}, rand , function(){
			//alert('oi')
		})	

	}

	// Pegar posi��o da pe�a
	this.position = function( x, y){

		//var px = ( me.icon_size.p *x) + ( me.icon_size.w * x )  ;
		//var py = ( me.icon_size.p *y) + ( me.icon_size.h * y )  ;		
		var px = me.icon_size.w * x ;
		var py = me.icon_size.h * y ;		
		return { 'px': px, 'py': py }
	}

	// Pegar Div da posi��o clicada
	this.getPic = function( x, y  ){
		var pic = where.find("[data-coord="+ String(x)+String(y) +"]");		
		return pic;
	}

	// Ajuda
	this.help = function( type ){

		if( me.help_num ){
			switch( type ){
				// Girar para Direita
				case 'RR':

					me.rotation( 'R' )

					break;
				// Girar para Esquerda
				case 'RL':

					me.rotation( 'L' )

					break;
				// Embaralhar
				case 'X':

					me.twist()

					break;
			}

			me.help_num--
			me.displayLabelChances.html( me.help_num )
			if( !me.help_num ){
				if( me.onEndChanges ){
					me.onEndChanges();
				}
			}
	
		}

		me.isTheEnd()

	}

	// Girar
	this.rotation = function( dir ){

		var new_matrix = [];
		var lin = me.rows;
		var col = me.cols;

		switch( dir ){
			case 'R':
				

				for( var a = 0, l = 0; l < lin; l++, a ++ ){
					new_matrix[l] = [];
					for( var b = (lin-1), c=0; c < col; c++, b-- ){
						new_matrix[l][c] = me.matrix[b][a];
					}
				}


				break;
			case 'L':

				for( var a = (col-1), l = 0; l < lin; l++, a -- ){
					new_matrix[l] = [];
					for( var b = 0, c=0; c < col; c++, b++ ){
						new_matrix[l][c] = me.matrix[b][a];
					}
				}

			break;

		}

		
		me.matrix = new_matrix;
		me.justify();
		me.anime( where )
		
		console.log( new_matrix )

	}

	// Embaralhar
	this.twist = function(){
		
		var ar = [];
		for( var l = 0; l < me.matrix.length ; l++  ){
			for( var c = 0; c < me.matrix[l].length; c++ ){
				if( me.matrix[l][c].num != 'X' ) ar.push( me.matrix[l][c] );
			}
		}

		var new_deck = me.shuffle_ar( ar );

		for( var l = 0; l < me.matrix.length ; l++  ){
			for( var c = 0; c < me.matrix[l].length; c++ ){
				if( me.matrix[l][c].num != 'X' ){
					me.matrix[l][c] = ar[0];
					ar.shift();
				} 
			}
		}

		me.anime( where )

	}

	// Verifica se acabou 
	this.isTheEnd = function(){

		var ps = []
		var end = true;
		var test = [];

		for( var l = 0; l < me.matrix.length ; l++  ){
			for( var c = 0; c < me.matrix[l].length; c++ ){

				var num = me.matrix[l][c].num;
				if( num != 'X' ){
					if( !ps[num] ){
						ps[num] = 1;	
					}  else {
						ps[num] ++;	
					}						

					if( ps[num] >= 3 ){
						//me.matrix[l][c].element.css('border','solid 1px')
						test.push({ 'x' : c, 'y' : l })	
						end = false;
					}
				}
			}
		}
		
		if( !end ){
			end = true;
			//console.log( test )
			// Test combination
			for( var i in test ){				
				if( me.testShoot( Number(test[i].x), Number(test[i].y)) ){
					if( me.count >= me.num_comb ){
						end = false;
						break;
					} 						
				}
			}

			if( me.help_num ) end = false;
		}

		//me.view( $('#view') );	

		me.status['amounts'] = ps;
		if(me.onStatus)me.onStatus( me.status )
		me.remove = 0;
		me.remove = []

		if( end ){
			if( me.level == me.max_level ){
				me.gameover()
			} else {
				me.endLevel();
				
			}
		}
		
	}

	// Seta os pontos
	this.setScore = function( v ){

		var P = (me.point * v) ;
		P += (P * v) / 50; 
		me.score += P;
		me.displayScore();

	}	

	// Errou!
	this.setOut = function(){

		me.score -= me.point;
		if( me.score < 0 ) me.score = 0;
		me.displayScore();

	}

	// Mostra pontos
	this.displayScore = function(){
		if( me.displayLabelScore ) me.displayLabelScore.val( me.score )
	}

	// Mostra Level
	this.displayLevel = function(){
		if( me.displayLabelLevel ) me.displayLabelLevel.val( me.level )
	}
	
	// Fim do Level
	this.endLevel = function(){
		if( me.onEndLevel ) me.onEndLevel();
		/*if( confirm("Go Next Level?") ){
			me.nextLevel()
		}*/

	}

	// Pr�ximo Level
	this.nextLevel = function(){
		me.level ++;
		me.cols ++;
		me.rows ++;
		me.help_num = 3;
		me.gaming = false;
		me.displayLevel()
		me.start()
	}

	// Acabou o jogo
	this.gameover = function(){
		me.gaming = false;
		if( me.onGameOver ) me.onGameOver()
	}
	
	// 
	this.save = function(){
		localStorage.setItem('matrix',JSON.stringify(me.matrix) )
	}
	this.load = function(){
		me.matrix =  JSON.parse( localStorage.getItem('matrix') )
		me.draw(where);
	}



}