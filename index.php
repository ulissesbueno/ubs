
<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css">

<link rel="stylesheet" type="text/css" href="css/main.css?1">

<!-- 
Theme do jogo : sport, food, music
-->
<link rel="stylesheet" type="text/css" href="themes/sport/theme.css">


<script type="text/javascript" src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
<script type="text/javascript" src='js/jquery-ui.min.js'></script>
<script src='js/ubs.js?<?php echo uniqid() ?>'></script>
<script src='js/app.js?<?php echo uniqid() ?>'></script>

<div id='main'>
	<div class="top">
		HOTFIX - Puzzle
	</div>
	<div class="content">
		<div id="GameOver">
			<h2>GAME OVER</h2>
			<ul>
				<li class="restart"> Recomeçar </li>
			</ul>
		</div>
		<div id="StopLevel">
			<h2>Parabéns!</h2>
			<ul>
				<li class="next"> Próximo Level </li>
				<li class="restart"> Recomeçar </li>	
			</ul>
		</div>
		<div id='game'></div>
		<div id='status'>
			<div class="header" >
				Status
			</div>
			<div >
				<div class="score">
					<label>Score</label> <input type="text" id="score" readonly="readonly">
				</div>
				<div class="level">
					<label>Level</label> <input type="text" id="level" readonly="readonly">
				</div>
				<div class="pices">
					
				</div>
				<div style='clear:both'></div>
			</div>
			<div style='clear:both'></div>
		</div>
		<div style='clear:both'></div>
	</div>
	<div class="footer">
		<ul>
			<li> <div class="icon help-rr"></div> </li>
			<li> <div class="icon help-rl"></div> </li>
			<li> <div class="icon help-x"></div> </li>
			<li> <div class="icon counter"></div> </li>
		</ul>
	</div>
	
</div>
<div id='view'></div>
